import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imagenes',
  templateUrl: './imagenes.component.html',
  styleUrls: ['./imagenes.component.scss']
})
export class ImagenesComponent implements OnInit {

  constructor() { }

  images = [
    { img: "/assets/img/imagen-1.jpg", thumb: "/assets/img/imagen-1.jpg", description: "Image 1" },
    { img: "/assets/img/imagen-2.jpg", thumb: "/assets/img/imagen-2.jpg", description: "Image 2" },
    { img: "/assets/img/imagen-3.jpg", thumb: "/assets/img/imagen-3.jpg", description: "Image 3" },
    { img: "/assets/img/imagen-4.jpg", thumb: "/assets/img/imagen-4.jpg", description: "Image 4" },
    { img: "/assets/img/imagen-5.jpg", thumb: "/assets/img/imagen-5.jpg", description: "Image 5" },
    { img: "/assets/img/imagen-6.jpg", thumb: "/assets/img/imagen-6.jpg", description: "Image 6" },
    { img: "/assets/img/imagen-7.jpg", thumb: "/assets/img/imagen-7.jpg", description: "Image 7" },
    { img: "/assets/img/imagen-8.jpg", thumb: "/assets/img/imagen-8.jpg", description: "Image 8" },
    { img: "/assets/img/imagen-9.jpg", thumb: "/assets/img/imagen-9.jpg", description: "Image 9" },
  ];

  ngOnInit() {
  }

}
