import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentsComponent } from './components.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MDBSpinningPreloader, MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { ImagenesComponent } from './imagenes/imagenes.component';
import { LoginComponent } from './login/login.component';
import { RegistrarComponent } from './registrar/registrar.component';

@NgModule({
    declarations: [
        ComponentsComponent,
        ImagenesComponent,
        LoginComponent,
        RegistrarComponent
    ],
    imports: [
        ToastModule.forRoot(),
        MDBBootstrapModulesPro.forRoot(),
        AgmCoreModule.forRoot({
            // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
            apiKey: 'Your_api_key'
        }),
        BrowserModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
    ],
    providers: [MDBSpinningPreloader],
    schemas: [NO_ERRORS_SCHEMA],

})
export class ComponentsModule { }