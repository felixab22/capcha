import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ComponentsComponent } from './components/components.component';
import { ImagenesComponent } from './components/imagenes/imagenes.component';
import { RegistrarComponent } from './components/registrar/registrar.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: LoginComponent },
    {
        path: 'cs',
        component: ComponentsComponent,
        // canActivate: [AuthGuard],
        children:
            [
                { path: 'Imagenes', component: ImagenesComponent },
                { path: 'Registrar', component: RegistrarComponent }                
            ]
    },
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
